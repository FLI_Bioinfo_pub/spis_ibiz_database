SPIs_IBIZ_database is a customized database created by Abricate and ready to be used by Abricate (https://github.com/tseemann/abricate). It comprisses sequences for _Salmonella_ Pathogenicity Islands (SPIs). 

The sequences were extracted from the following sources:

1.	SPIs database online (PAIDB): http://www.paidb.re.kr/browse_pais.php?%20m=p#Salmonellaenterica

2.	Publications:

- Cohen E, Rahav G, Gal-Mor O. Genome Sequence of an Emerging Salmonella enterica Serovar Infantis and Genomic Comparison with Other S. Infantis Strains. Genome Biol Evol. 2020;12(3):151-159. doi:10.1093/gbe/evaa048 https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7144548/

- Sévellec Y, Vignaud ML, Granier SA, et al. Polyphyletic Nature of Salmonella enterica Serotype Derby and Lineage-Specific Host-Association Revealed by Genome-Wide Analysis. Front Microbiol. 2018;9:891. Published 2018 May 17. doi:10.3389/fmicb.2018.00891 https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5966662/

- Chih-Hao Hsu, Cong Li, Maria Hoffmann, Patrick McDermott, Jason Abbott, Sherry Ayers, Gregory H. Tyson, Heather Tate, Kuan Yao, Marc Allard, and Shaohua Zhao.Microbial Drug Resistance.Oct 2019.1238-1249.http://doi.org/10.1089/mdr.2019.0045